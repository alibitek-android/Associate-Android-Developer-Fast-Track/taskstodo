import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import ro.linuxgeek.taskstodo.MainActivity;
import ro.linuxgeek.taskstodo.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mMainActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    // XTODO: Write a UI test to validate that clicking the Add floating action button results in displaying the AddTaskActivity.
    @Test
    public void addNoteToNotesList() throws Exception {
        onView(withId(R.id.fab)).perform(click());
        onView(withId(R.id.text_input_description)).check(matches(isDisplayed()));
        onView(withId(R.id.switch_priority)).check(matches(isDisplayed()));
        onView(withId(R.id.text_date)).check(matches(isDisplayed()));
        onView(withId(R.id.select_date)).check(matches(isDisplayed()));
    }
}
