package ro.linuxgeek.taskstodo;

import android.content.ContentValues;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import ro.linuxgeek.taskstodo.data.DatabaseContract;
import ro.linuxgeek.taskstodo.data.Task;
import ro.linuxgeek.taskstodo.data.TaskAdapter;
import ro.linuxgeek.taskstodo.data.TaskUpdateService;

import java.util.Calendar;
import java.util.Objects;

import ro.linuxgeek.taskstodo.R;

public class MainActivity extends AppCompatActivity implements
        TaskAdapter.OnItemClickListener,
        View.OnClickListener,
        LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = MainActivity.class.getSimpleName();
    private static final int ID_TASKS_LOADER = 42;
    private TaskAdapter mAdapter;
    private RecyclerView mRecyclerView;
    private int mPosition = RecyclerView.NO_POSITION;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.d(TAG, "onCreate");

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mAdapter = new TaskAdapter(null, this);
        mAdapter.setOnItemClickListener(this);

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        // Set up floating action button
        FloatingActionButton fab =
                (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(this);

        getSupportLoaderManager().initLoader(ID_TASKS_LOADER, null, this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /* Click events in Floating Action Button */
    @Override
    public void onClick(View v) {
        Log.d(TAG, "FAB clicked!");
        Intent intent = new Intent(this, AddTaskActivity.class);
        startActivity(intent);
    }

    /* Click events in RecyclerView items */
    @Override
    public void onItemClick(View v, int position) {
        //XTODO: Handle list item click event
        Log.d(TAG, "onItemClick - position: " + position);

        Intent intent = new Intent(MainActivity.this, TaskDetailActivity.class);
        intent.setData(DatabaseContract.makeUriForTaskId(mAdapter.getItem(position).getId()));
        startActivity(intent);
    }

    /* Click events on RecyclerView item checkboxes */
    @Override
    public void onItemToggled(boolean active, int position) {
        //XTODO: Handle task item checkbox event
        Log.d(TAG, "onItemToggled - active: " + active + ", position: " + position);
        Task tappedTask = mAdapter.getItem(position);
        tappedTask.setIsCompleted(active);

        ContentValues values = new ContentValues(1);
        values.put(DatabaseContract.TaskColumns.IS_COMPLETE, active);

        TaskUpdateService.updateTask(this, DatabaseContract.makeUriForTaskId(tappedTask.getId()), values);

        //mAdapter.notifyItemChanged(position, tappedTask);
        //mAdapter.notifyItemMoved(position, mAdapter.getItemCount() - 1);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Log.d(TAG, "onCreateLoader - id: " + id);

        //TestInsertTask();
        //TestUpdateTask("2");
        //TestDeleteTask("2");

        switch (id) {
            case ID_TASKS_LOADER:
                return new CursorLoader(this,
                        DatabaseContract.CONTENT_URI,
                        DatabaseContract.TASKS_COLUMNS,
                        null,
                        null,
                        getSortBy());

                //return TestGetTaskById("1");

            default:
                throw new UnsupportedOperationException("No implementation for loader: " + id);
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        Log.d(TAG, "onLoadFinished - id: " + loader.getId());

        mAdapter.swapCursor(data);

        if (mPosition == RecyclerView.NO_POSITION)
            mPosition = 0;

        mRecyclerView.smoothScrollToPosition(mPosition);

        if (data.getCount() != 0) {
            Log.d(TAG, "onLoadFinished - show tasks");
        }

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        Log.d(TAG, "onLoaderReset - id: " + loader.getId());
        mAdapter.swapCursor(null);
    }

    // foreground to background transition
    @Override
    public void onRestart() {
        Log.d(TAG, "onRestart");
        super.onRestart();

        //getSupportLoaderManager().restartLoader(ID_TASKS_LOADER, null, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");

        //getSupportLoaderManager().restartLoader(ID_TASKS_LOADER, null, this);
    }


    String getSortBy() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String sortByValue = preferences.getString(getString(R.string.pref_sortBy_key), getString(R.string.pref_sortBy_default));

        if (Objects.equals(sortByValue, getString(R.string.pref_sortBy_default))) {
            Log.d(TAG, "Default sorting");
            return DatabaseContract.DEFAULT_SORT;
        } else {
            Log.d(TAG, "Due date sorting");
            return DatabaseContract.DATE_SORT;
        }
    }

    CursorLoader TestGetTaskById(String id) {
        return new CursorLoader(this,
            DatabaseContract.makeUriForTaskId(id),
            DatabaseContract.TASKS_COLUMNS,
            null,
            null,
            null);
    }

    void TestInsertTask() {
        ContentValues values = new ContentValues(4);
        values.put(DatabaseContract.TaskColumns.DESCRIPTION, "Become an Associate Android Developer");
        values.put(DatabaseContract.TaskColumns.IS_PRIORITY, 1);
        values.put(DatabaseContract.TaskColumns.IS_COMPLETE, 0);

        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 9, 16, 10, 34, 0);

        values.put(DatabaseContract.TaskColumns.DUE_DATE, calendar.getTimeInMillis());

        TaskUpdateService.insertNewTask(this, values);
    }

    void TestUpdateTask(String id) {
        ContentValues values = new ContentValues(4);
        values.put(DatabaseContract.TaskColumns.DESCRIPTION, "Updated Become an Associate Android Developer");
        values.put(DatabaseContract.TaskColumns.IS_PRIORITY, 1);
        values.put(DatabaseContract.TaskColumns.IS_COMPLETE, 1);

        Calendar calendar = Calendar.getInstance();
        calendar.set(2017, 9, 16, 10, 34, 0);

        values.put(DatabaseContract.TaskColumns.DUE_DATE, calendar.getTimeInMillis());

        TaskUpdateService.updateTask(this, DatabaseContract.makeUriForTaskId(id), values);
    }

    void TestDeleteTask(String id) {
        TaskUpdateService.deleteTask(this, DatabaseContract.makeUriForTaskId(id));
    }
}
