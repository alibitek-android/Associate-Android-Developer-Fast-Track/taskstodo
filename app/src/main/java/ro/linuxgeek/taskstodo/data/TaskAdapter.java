package ro.linuxgeek.taskstodo.data;

import android.content.Context;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import ro.linuxgeek.taskstodo.R;
import ro.linuxgeek.taskstodo.views.TaskTitleView;

import java.util.List;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.TaskHolder> {
    private static final String TAG = TaskAdapter.class.getSimpleName();

    /* Callback for list item click events */
    public interface OnItemClickListener {
        void onItemClick(View v, int position);

        void onItemToggled(boolean active, int position);
    }

    /* ViewHolder for each task item */
    public class TaskHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TaskTitleView nameView;
        public TextView dateView;
        public ImageView priorityView;
        public CheckBox checkBox;

        public TaskHolder(View itemView) {
            super(itemView);

            nameView = (TaskTitleView) itemView.findViewById(R.id.text_description);
            dateView = (TextView) itemView.findViewById(R.id.text_date);
            priorityView = (ImageView) itemView.findViewById(R.id.priority);
            checkBox = (CheckBox) itemView.findViewById(R.id.checkbox);

            itemView.setOnClickListener(this);
            checkBox.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (v == checkBox) {
                completionToggled(this);
            } else {
                postItemClick(this);
            }
        }
    }

    private Cursor mCursor;
    private OnItemClickListener mOnItemClickListener;
    private Context mContext;

    public TaskAdapter(Cursor cursor, OnItemClickListener itemClickListener) {
        mCursor = cursor;
        mOnItemClickListener = itemClickListener;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mOnItemClickListener = listener;
    }

    private void completionToggled(TaskHolder holder) {
        int adapterPosition = holder.getAdapterPosition();

        int taskStatus;

        if (holder.checkBox.isChecked()) {
            taskStatus = TaskTitleView.DONE;
        } else {
            taskStatus = TaskTitleView.NORMAL;
        }

        if (holder.nameView.getState() != taskStatus) {
            holder.nameView.setState(taskStatus);
            //notifyItemChanged(adapterPosition);
        }

        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemToggled(holder.checkBox.isChecked(), adapterPosition);
        }
    }

    private void postItemClick(TaskHolder holder) {
        if (mOnItemClickListener != null) {
            mOnItemClickListener.onItemClick(holder.itemView, holder.getAdapterPosition());
        }
    }

    @Override
    public TaskHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.d(TAG, "onCreateViewHolder - viewType: " + viewType);

        mContext = parent.getContext();

        View itemView = LayoutInflater.from(mContext).inflate(R.layout.list_item_task, parent, false);

        return new TaskHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TaskHolder holder, int position) {
        Log.d(TAG, "onBindViewHolder - position: " + position);

        //XTODO: Bind the task data to the views
        mCursor.moveToPosition(position);

        Task task = new Task(mCursor);

        bindViewHolder(holder, task);
    }

    @Override
    public void onBindViewHolder(TaskHolder holder, int position, List<Object> payloads) {
        if (!payloads.isEmpty()) {
            if (payloads.get(0) instanceof Task) {
                Task task = (Task) payloads.get(0);
                bindViewHolder(holder, task);
            }
        } else {
            super.onBindViewHolder(holder, position, payloads);
        }
    }

    private void bindViewHolder(TaskHolder holder, Task task) {
        Log.d(TAG, "onBindViewHolder: isComplete: " + task.isComplete());

        // Completed
        holder.checkBox.setChecked(task.isComplete());

        // Name
        holder.nameView.setText(task.getDescription());

        int taskStatus = TaskTitleView.NORMAL;
        if (task.isComplete()) {
            taskStatus = TaskTitleView.DONE;
        }
        else if ( task.hasDueDate() && (task.getDueDateMillis() < System.currentTimeMillis()) ) {
            taskStatus = TaskTitleView.OVERDUE;
        }
        holder.nameView.setState(taskStatus);

        // Due Date
        if (task.hasDueDate()) {
            holder.dateView.setText(DateUtils.getRelativeTimeSpanString(task.getDueDateMillis()));
            holder.dateView.setVisibility(View.VISIBLE);
        } else {
            holder.dateView.setText("");
            holder.dateView.setVisibility(View.GONE);
        }

        // Priority Indicator Image
        Drawable imagePriority;
        if (task.isPriority()) {
            imagePriority = mContext.getDrawable(R.drawable.ic_priority);
        } else {
            imagePriority = mContext.getDrawable(R.drawable.ic_not_priority);
        }

        holder.priorityView.setImageDrawable(imagePriority);
    }

    @Override
    public int getItemCount() {
        return (mCursor != null) ? mCursor.getCount() : 0;
    }

    /**
     * Retrieve a {@link Task} for the data at the given position.
     *
     * @param position Adapter item position.
     *
     * @return A new {@link Task} filled with the position's attributes.
     */
    public Task getItem(int position) {
        if (!mCursor.moveToPosition(position)) {
            throw new IllegalStateException("Invalid item position requested");
        }

        return new Task(mCursor);
    }

    @Override
    public long getItemId(int position) {
        return getItem(position).getId();
    }

    public void swapCursor(Cursor cursor) {
        if (mCursor != null) {
            mCursor.close();
        }

        mCursor = cursor;
        notifyDataSetChanged();
    }
}
