package ro.linuxgeek.taskstodo.data;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;

public class TaskProvider extends ContentProvider {
    private static final String TAG = TaskProvider.class.getSimpleName();

    private static final int CLEANUP_JOB_ID = 43;

    private static final int TASKS = 100;
    private static final int TASKS_WITH_ID = 101;

    private TaskDbHelper mDbHelper;

    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        // content://ro.linuxgeek.taskstodo/tasks
        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY, DatabaseContract.TABLE_TASKS, TASKS);

        // content://ro.linuxgeek.taskstodo/tasks/id
        sUriMatcher.addURI(DatabaseContract.CONTENT_AUTHORITY, DatabaseContract.TABLE_TASKS + "/#", TASKS_WITH_ID);
    }

    @Override
    public boolean onCreate() {
        mDbHelper = new TaskDbHelper(getContext());
        manageCleanupJob();
        return true;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null; /* Not used */
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        Cursor cursor;
        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        //XTODO: Implement task query
        //XTODO: Expected "query all" Uri: content://ro.linuxgeek.taskstodo/tasks
        //XTODO: Expected "query one" Uri: content://ro.linuxgeek.taskstodo/tasks/{id}
        switch (sUriMatcher.match(uri)) {
            case TASKS:
                cursor = db.query(DatabaseContract.TABLE_TASKS,
                        projection, // columns
                        selection, // selection
                        selectionArgs, // selectionArgs
                        null, // group by
                        null, // having
                        sortOrder // orderby
                );
                break;

            case TASKS_WITH_ID:
                long id = ContentUris.parseId(uri);
                selection = String.format("%s = ?", DatabaseContract.TaskColumns._ID);
                selectionArgs = new String[]{String.valueOf(id)};

                cursor = db.query(DatabaseContract.TABLE_TASKS,
                                    projection,
                                    selection,
                                    selectionArgs,
                                    null,
                                    null,
                                    sortOrder);
                break;

            default:
                throw new IllegalArgumentException("Illegal query URI: " + uri);
        }

        Context context = getContext();
        if (context != null){
            cursor.setNotificationUri(context.getContentResolver(), uri);
        }

        return cursor;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, ContentValues values) {
        //XTODO: Implement new task insert
        //XTODO: Expected Uri: content://ro.linuxgeek.taskstodo/tasks
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        Uri returnUri;

        switch (sUriMatcher.match(uri)) {
            case TASKS:
                long id = db.insert(DatabaseContract.TABLE_TASKS, null, values);
                returnUri = DatabaseContract.CONTENT_URI.buildUpon().appendPath(String.valueOf(id)).build();
                Log.d(TAG, "insert: uri: " + returnUri.toString());

                if (id != -1) {
                    Context context = getContext();
                    if (context != null) {
                        context.getContentResolver().notifyChange(uri, null);
                    }
                }

                break;

            default:
                throw new IllegalArgumentException("Unknown insert URI:" + uri);
        }


        return returnUri;
    }

    @Override
    public int update(@NonNull Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        //XTODO: Implement existing task update
        //XTODO: Expected Uri: content://ro.linuxgeek.taskstodo/tasks/{id}
        SQLiteDatabase db = mDbHelper.getReadableDatabase();
        int numRowsUpdated;

        switch (sUriMatcher.match(uri)) {
            case TASKS_WITH_ID:
                long id = ContentUris.parseId(uri);
                selection = String.format("%s = ?", DatabaseContract.TaskColumns._ID);
                selectionArgs = new String[]{String.valueOf(id)};
                numRowsUpdated = db.update(DatabaseContract.TABLE_TASKS, values, selection, selectionArgs);
                break;

            default:
                throw new IllegalArgumentException("Unknown insert URI:" + uri);
        }

        if (numRowsUpdated != 0) {
            Context context = getContext();
            if (context != null){
                context.getContentResolver().notifyChange(uri, null);
            }
        }

        return numRowsUpdated;
    }

    @Override
    public int delete(@NonNull Uri uri, String selection, String[] selectionArgs) {

        switch (sUriMatcher.match(uri)) {
            case TASKS:
                //Rows aren't counted with null selection
                selection = (selection == null) ? "1" : selection;
                break;
            case TASKS_WITH_ID:
                long id = ContentUris.parseId(uri);
                selection = String.format("%s = ?", DatabaseContract.TaskColumns._ID);
                selectionArgs = new String[]{String.valueOf(id)};
                break;
            default:
                throw new IllegalArgumentException("Illegal delete URI");
        }

        SQLiteDatabase db = mDbHelper.getWritableDatabase();
        int numRowsDeleted = db.delete(DatabaseContract.TABLE_TASKS, selection, selectionArgs);

        if (numRowsDeleted != 0) {
            Context context = getContext();
            if (context != null){
                context.getContentResolver().notifyChange(uri, null);
            }
        }

        return numRowsDeleted;
    }

    /* Initiate a periodic job to clear out completed items */
    private void manageCleanupJob() {
        Log.d(TAG, "Scheduling cleanup job");
        JobScheduler jobScheduler = (JobScheduler) getContext()
                .getSystemService(Context.JOB_SCHEDULER_SERVICE);

        //Run the job approximately every hour
        long jobInterval = 3600000L;

        ComponentName jobService = new ComponentName(getContext(), CleanupJobService.class);
        JobInfo task = new JobInfo.Builder(CLEANUP_JOB_ID, jobService)
                .setPeriodic(jobInterval)
                .setPersisted(true)
                .build();

        if (jobScheduler.schedule(task) != JobScheduler.RESULT_SUCCESS) {
            Log.w(TAG, "Unable to schedule cleanup job");
        }
    }
}
