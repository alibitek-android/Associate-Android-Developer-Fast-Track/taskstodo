package ro.linuxgeek.taskstodo.views;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import ro.linuxgeek.taskstodo.R;

public class ErrorDialogFragment extends DialogFragment {
    private String mErrorMessage;
    private static final String ID_ERROR_MESSAGE = "error_message";

    public static ErrorDialogFragment newInstance(String errorMessage) {
        ErrorDialogFragment f = new ErrorDialogFragment();

        Bundle args = new Bundle();
        args.putString(ID_ERROR_MESSAGE, errorMessage);
        f.setArguments(args);

        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mErrorMessage = getArguments().getString(ID_ERROR_MESSAGE);
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage(mErrorMessage)
                .setTitle(R.string.error_dialog_title);

        return builder.create();
    }
}

