package ro.linuxgeek.taskstodo;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import ro.linuxgeek.taskstodo.data.DatabaseContract;
import ro.linuxgeek.taskstodo.data.Task;
import ro.linuxgeek.taskstodo.data.TaskUpdateService;
import ro.linuxgeek.taskstodo.reminders.AlarmScheduler;
import ro.linuxgeek.taskstodo.views.DatePickerFragment;
import ro.linuxgeek.taskstodo.views.ErrorDialogFragment;
import ro.linuxgeek.taskstodo.views.TaskTitleView;

import java.util.Calendar;

public class TaskDetailActivity extends AppCompatActivity implements
        DatePickerDialog.OnDateSetListener {

    private static final String TAG = TaskDetailActivity.class.getSimpleName();

    private TaskTitleView mNameView;
    private TextView mDateView;
    private ImageView mPriorityView;
    private Task mTask;
    private Uri mTaskUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Task must be passed to this activity as a valid provider Uri
        mTaskUri = getIntent().getData();

        //XTODO: Display attributes of the provided task in the UI
        if (mTaskUri != null) {
            mTask = getTask(mTaskUri);

            if (mTask != null) {
                Log.d(TAG, "onCreate - task: " + mTask);

                setContentView(R.layout.activity_detail);

                mNameView = (TaskTitleView) findViewById(R.id.detail_text_description);
                mDateView = (TextView) findViewById(R.id.detail_text_date);
                mPriorityView = (ImageView) findViewById(R.id.detail_priority);

                // Name
                mNameView.setText(mTask.getDescription());

                int taskStatus = TaskTitleView.NORMAL;
                if (mTask.isComplete()) {
                    taskStatus = TaskTitleView.DONE;
                } else if (mTask.hasDueDate() && (mTask.getDueDateMillis() > System.currentTimeMillis())) {
                    taskStatus = TaskTitleView.OVERDUE;
                }
                mNameView.setState(taskStatus);

                // Due Date
                if (mTask.hasDueDate()) {
                    mDateView.setText(DateUtils.getRelativeTimeSpanString(mTask.getDueDateMillis()));
                } else {
                    mDateView.setText(getString(R.string.date_empty));
                }

                // Priority Indicator Image
                Drawable imagePriority;
                if (mTask.isPriority()) {
                    imagePriority = getDrawable(R.drawable.ic_priority);
                } else {
                    imagePriority = getDrawable(R.drawable.ic_not_priority);
                }

                mPriorityView.setImageDrawable(imagePriority);
            }
        }
    }

    private Task getTask(Uri uri) {
        Cursor cursor = getContentResolver().query(uri,
                                                    DatabaseContract.TASKS_COLUMNS,
                                                    null,
                                                    null,
                                                    null);

        Task task = null;

        if (cursor != null && cursor.moveToFirst()) {
            task = new Task(cursor);
            cursor.close();
        }

        return task;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_task_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_delete) {
            Log.d(TAG, "onOptionsItemSelected - delete - Task ID: " + mTask.getId());

            TaskUpdateService.deleteTask(this, DatabaseContract.makeUriForTaskId(mTask.getId()));

            NavUtils.navigateUpFromSameTask(this);

            return true;
        }

        if (id == R.id.action_reminder) {
            Log.d(TAG, "onOptionsItemSelected - reminder - Task ID: " + mTask.getId());

            if (!mTask.isComplete()) {
                showDatePicker();
            }
            else {
                ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(getString(R.string.error_reminder_for_completed_task));
                errorDialogFragment.show(getSupportFragmentManager(), "error_reminder_date");
            }

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDatePicker() {
        DialogFragment datePickerFragment = new DatePickerFragment();
        datePickerFragment.show(getSupportFragmentManager(), "reminder");
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        //XTODO: Handle date selection from a DatePickerFragment

        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        calendar.set(Calendar.HOUR_OF_DAY, 12);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);

        Log.d(TAG, "reminder - onDateSet - "
                + "year: " + calendar.get(Calendar.YEAR)
                + ", month: " + calendar.get(Calendar.MONTH)
                + ", day: " + calendar.get(Calendar.DAY_OF_MONTH)
                + ", hour: " + calendar.get(Calendar.HOUR_OF_DAY)
                + ", minute: " + calendar.get(Calendar.MINUTE)
                + ", second: " + calendar.get(Calendar.SECOND)
        );

        long reminderDateMillis = calendar.getTimeInMillis();

        boolean reminderIsLowerThanDueDateTimestamp = reminderDateMillis <= mTask.getDueDateMillis();
        boolean reminderIsHigherThanCurrentTimestamp = reminderDateMillis >= System.currentTimeMillis();

        int errorMessage = -1;
        if (!reminderIsHigherThanCurrentTimestamp) {
            errorMessage = R.string.date_picker_past_date_error_dialog_message;
        } else if (!reminderIsLowerThanDueDateTimestamp) {
            errorMessage = R.string.date_picker_future_date_error_dialog_message;
        }

        if (errorMessage != -1) {
            ErrorDialogFragment errorDialogFragment = ErrorDialogFragment.newInstance(getString(errorMessage));
            errorDialogFragment.show(getSupportFragmentManager(), "error_reminder_date");
        } else {
            AlarmScheduler.scheduleAlarm(this, reminderDateMillis, mTaskUri);
        }
    }
}
